$(document).ready(function($) {
	var pages = parseInt($('#pages').val());
	var page = 2;
	var next = true;
	var blog = window.location.origin;
	var $timeline = $('.timeline');
	var currentPosts = $('#length').val();
	var type = $('#type').val();
	var options = {};
	if (type == 'posts') {
		 options = {limit: 5, page: page, include: 'tags'};
	} else if (type == 'tags'){
		var slug = $('#slug').val();
		options = {include: 'tags', filter: 'tags:['+slug+']'};
	}
	$timeline.scroll(function(event) {
		if ($timeline.scrollLeft()+$(window).width() == $timeline.prop('scrollWidth') && page != null) {
			$.get(ghost.url.api('posts', options)).done(function (data){
				console.log(data);
			  var posts= data.posts;
		   	page = data.meta.pagination.next;
		   	for (var i = 0; i < posts.length; i++){
		   		tags = [];
		   		var tags = posts[i].tags.map(function(e, i) {
		   	 		return '<a href="/tag/'+e.slug+'">'+e.name+'</a>';
		   		});
					var time = new Date(posts[i].published_at.substring(0,10));
					time = time.toString().split(' ');
			  	var html = '<article class="post loop">'+
									    '<a href="'+posts[i].url+'">'+
									        '<div class="outside-post">'+
									            '<div class="circle"></div>'+
									            '<div class="inside-post">'+
									                '<div class="post-image">'+
									                    '<img src="'+posts[i].image+'" alt="'+posts[i].title+'">'+
									                '</div>'+
									                '<header class="post-header">'+
									                    '<h2 class="post-title">'+posts[i].title+'</h2>'+
									                '</header>'+
									                '<section class="post-excerpt">'+
									                    '<p>'+posts[i].markdown.substring(0, 100).match(/([A-z ])\w+/g).join(' ')+'...</p>'+
									                '</section>'+
									                '<footer class="post-meta">'+
									                		'Tags: '+ tags.slice(0,4).join(' ') +
									                    '<time class="post-date">'+time[2]+' '+time[1]+' '+time[3]+'</time>'+
									                '</footer>'+
									            '</div>'+
									        '</div> '+
									    '</a>'+
									'</article>';
					if (type == 'posts' || i>=currentPosts)	$timeline.append(html)
			  }
			}).fail(function (err){
			  console.log(err);
			});
		}
	});
});